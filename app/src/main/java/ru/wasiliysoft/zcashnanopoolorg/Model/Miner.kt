package ru.wasiliysoft.zcashnanopoolorg.Model

import java.io.Serializable


// Created by WasiliySoft on 20.01.2019.
data class Miner(val name: String, val ticker: String, val account: String) : Serializable {
    @Transient
    var gen: NpGeneral.Data? = null
    @Transient
    var calc: NpCalc.Data? = null
}